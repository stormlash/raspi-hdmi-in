#gst-launch-1.0 v4l2src ! "video/x-raw,framerate=30/1,format=UYVY" ! v4l2h264enc extra-controls="controls,h264_profile=4,h264_level=10,video_bitrate=256000;" ! video/x-h264,profile=high ! h264parse ! queue ! matroskamux ! filesink location=foo.mkv
#gst-launch-1.0 v4l2src device=/dev/video0 ! 'video/x-raw,framerate=30/1,format=UYVY' ! v4l2h264enc ! 'video/x-h264,level=(string)4' ! decodebin ! videoconvert ! autovideosink

#This works but is slow
#gst-launch-1.0 v4l2src ! decodebin ! videoconvert ! autovideosink

#This works and is less slow but still too slow
#gst-launch-1.0 v4l2src ! decodebin ! videoconvert ! fbdevsink

#This maybe is less slow?
#gst-launch-1.0 v4l2src ! "video/x-raw,framerate=0/1,format=UYVY" ! videoconvert ! fbdevsink

#This works, but with some lag
#gst-launch-1.0 v4l2src ! "video/x-raw,framerate=30/1,format=UYVY" ! videoflip method=vertical-flip ! videoconvert ! fbdevsink sync=false

gst-launch-1.0 v4l2src ! "video/x-raw,framerate=15/1,format=UYVY" ! videoflip method=vertical-flip ! videoconvert ! fbdevsink sync=false

#gst-launch-1.0 v4l2src ! "video/x-raw,framerate=0/1,format=UYVY" ! videoconvert ! x264enc tune=zerolatency speed-preset=superfast ! h264parse ! fbdevsink sync=false

